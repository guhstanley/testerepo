sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast"
], function(Controller, MessageToast) {
	"use strict";

	return Controller.extend("br.com.Coletor.controller.DadosAR", {
		onNavBack: function(eEvent) {
			if (this._oRouter !== undefined) {
				var oData = {

				};
				var oViewModel = new sap.ui.model.json.JSONModel(oData);
				this.getView().setModel(oViewModel, "view");
				this._oRouter.navTo("-1");
			}
		},
		/**/
		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf br.com.Coletor.view.PageTwo
		 */
		onInit: function() {
			this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			debugger;
			this._oRouter.getRoute("dadosAR").attachPatternMatched(this.onAttachRouteMatched, this);
		},

		onGoItem: function(eEvent) {
			var oMaterial = this.getView().byId("idInptMaterial").getValue();
			if (oMaterial !== undefined) {
				this._oRouter.navTo("itemAR", {
					forn: this._Forn,
					item: oMaterial
				});
			} else {
				sap.m.MessageToast("Favor incluir um material");
			}
		},

		onAttachRouteMatched: function(oEvent) {
			debugger;
			if (oEvent.getParameters().name !== 'dadosAR') {
				return;
			}

			this.oForn = oEvent.getParameter("arguments").forn;

			var oView = this.getView();

			var oModel = oView.getModel("fornecimento");
			oView.setModel(oModel);

			//var sPath = '/Fornecimentos/0/';

			//oView.bindElement(sPath);

			var Fornecimentos = oModel.getData().Fornecimentos;
			var index = $.inArray(this.oForn, $.map(Fornecimentos, function(n) {
				return n.numForn;
			}));
			var sPath = "/Fornecimentos/" + index + "/";
			oView.bindElement(sPath);
			debugger;

			/*
			var oForn = oEvent.getParameters().arguments["Forn"];
			if (oForn !== undefined && oEvent.getParameters().name === 'dadosAR') {
				this._Forn = oForn;
				var oView = this.getView("br.com.Coletor.view.DadosAR");
				var oModel = oView.getModel("fornecimento");
				var oFornecimentos = oModel.getData().Fornecimentos;
				for (var i = 0; i < oFornecimentos.length; i++) {
					if (oFornecimentos[i].numForn == oForn) {
						var oData = {
							"numForn": oFornecimentos[i].numForn,
							"nfe": oFornecimentos[i].nfe,
							"dataNfe": oFornecimentos[i].dataNfe,
							"numVols": oFornecimentos[i].numVols
						};
						var oViewModel = new sap.ui.model.json.JSONModel(oData);
						oView.setModel(oViewModel, "view");
					}
				}

			}*/

		}

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf br.com.Coletor.view.PageTwo
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf br.com.Coletor.view.PageTwo
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf br.com.Coletor.view.PageTwo
		 */
		//	onExit: function() {
		//
		//	}

	});

});