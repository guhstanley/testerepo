sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("br.com.Coletor.controller.ValueHelp", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf br.com.Coletor.view.ValueHelp
		 */
		onInit: function() {
			this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this._oRouter.attachRouteMatched(this.onAttachRouteMatched, this);

		},
		onAttachRouteMatched: function() {


		},
		filterForn: function(evnt) {
			var oView = this.getView("br.com.Coletor.view.DadosAR");
			var oLista = oView.byId("lista");
			oLista.getBinding("items").filter(new sap.ui.model.Filter("numForn", sap.ui.model.FilterOperator.Contains, evnt.getParameter(
				"value")));

		},
		onBack: function() {

			this._oRouter.navTo("first");

		},
		onItemSelected: function(oEvent){
			debugger;
			var oValue = oEvent.getSource().getProperty("title");
			
			this._oRouter.navTo("firstWithForn", {
				forn: oValue
			});
		}

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf br.com.Coletor.view.ValueHelp
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf br.com.Coletor.view.ValueHelp
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf br.com.Coletor.view.ValueHelp
		 */
		//	onExit: function() {
		//
		//	}

	});

});