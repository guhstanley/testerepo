sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast"
], function(Controller, MessageToast) {
	"use strict";

	return Controller.extend("br.com.Coletor.controller.First", {
		onInit: function() {
			this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this._oRouter.attachRouteMatched(this.onAttachRouteMatched, this);
			
		},
		onAttachRouteMatched: function(oEvent) {
			var oData = {};
			
			debugger;
			var oView2 = this.getView();
			//oView2.bindElement("/Teste", "testemodel");
			oView2.setModel(this.getView().getModel("testemodel"));
			oView2.bindElement("/Teste");
			
			if (oEvent.getParameters().name === 'first') {
				oData = {
					"fieldFornec": ""
				};
			} else if (oEvent.getParameters().name === 'firstWithForn') {
				var oForn = oEvent.getParameters().arguments.forn;
				oData = {
					"fieldFornec": oForn
				};
			} else {
				return;
			}

			var oModel = new sap.ui.model.json.JSONModel(oData);
			var oView = this.getView("br.com.Coletor.view.First");
			oView.setModel(oModel, "view");
		},
		onAvan: function() {
			//Go to second page
			var oModel = this.getView().getModel("view");
			var oForn = oModel.getData().fieldFornec;

			this._oRouter.navTo("dadosAR", {
				forn: oForn
			});
		},
		onValueHelp: function() {
			this._oRouter.navTo("valuehelp");
		}
	});
});