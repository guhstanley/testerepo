sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"br/com/Coletor/model/models"
], function(UIComponent, Device, models) {
	"use strict";

	return UIComponent.extend("br.com.Coletor.Component", {

		metadata: {
			mani
			// create the views based on the url/hash
			this.getRouter().initialize();
		}
	});
});